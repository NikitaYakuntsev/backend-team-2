package ru.surf.hackathon.backend.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.surf.hackathon.backend.dto.LedResponse;
import ru.surf.hackathon.backend.exception.NoSuchLedException;
import ru.surf.hackathon.backend.mapper.LedMapper;
import ru.surf.hackathon.backend.repository.LedRepository;
import ru.surf.hackathon.backend.service.LedService;

@Service
@RequiredArgsConstructor
public class LedServiceImpl implements LedService {

    private final LedRepository ledRepository;
    private final LedMapper ledMapper;

    @Override
    public LedResponse findLedByBarcode(Long barcode) {
        return ledMapper.toDto(ledRepository.findByBarcode(barcode).orElseThrow(
                () -> new NoSuchLedException(barcode)
        ));
    }
}
