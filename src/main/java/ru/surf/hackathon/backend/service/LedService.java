package ru.surf.hackathon.backend.service;

import ru.surf.hackathon.backend.dto.LedResponse;

public interface LedService {

    LedResponse findLedByBarcode(Long barcode);
}
