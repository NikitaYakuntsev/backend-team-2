package ru.surf.hackathon.backend.mapper;

import ru.surf.hackathon.backend.dto.LedResponse;
import ru.surf.hackathon.backend.repository.entity.Led;

public interface LedMapper {
    LedResponse toDto(Led led);
    Led toEntity(LedResponse dto);
}
