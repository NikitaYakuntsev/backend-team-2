package ru.surf.hackathon.backend.exception;

public class BadBarcodeException extends RuntimeException{

    public BadBarcodeException() {
        super("Баркод должен быть положительным числом");
    }
}
