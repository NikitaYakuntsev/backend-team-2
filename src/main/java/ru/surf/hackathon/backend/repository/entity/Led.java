package ru.surf.hackathon.backend.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "led")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Led {
    //TODO: исправить типы
    @Id
    @Column(name = "no")
    private Integer id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "power_l")
    private Double power_l;

    @Column(name = "color_l")
    private Integer color_l;

    @Column(name = "lm_l")
    private Integer lm_l;

    @Column(name = "barcode")
    private Long barcode;

    @Column(name = "rub")
    private Integer rub;

    @Column(name = "p")
    private Double p;

    @Column(name = "lm")
    private Integer lm;

    @Column(name = "color")
    private Integer color;

    @Column(name = "rating")
    private Double rating;

    @Column(name = "lamp_image")
    private String lamp_image;

    @Column(name = "u")
    private String u;

    @Column(name = "life")
    private Integer life;


//    @Column(name = "matt")
//    private int matt;
//
//    @Column(name = "dim")
//    private double dim;
//
//    @Column(name = "eq_l")
//    private int eq_l;
//
//    @Column(name = "ra_l")
//    private int ra_l;
//
//
//    @Column(name = "pf_l")
//    private String pf_l;
//
//    @Column(name = "angle_l")
//    private String angle_l;
//
//
//    @Column(name = "war")
//    private int war;
//
//    @Column(name = "prod")
//    private int prod;
//
//    @Column(name = "w")
//    private int w;
//
//    @Column(name = "d")
//    private int d;
//
//    @Column(name = "h")
//    private int h;
//
//    @Column(name = "t")
//    private String t;
//
//    @Column(name = "plant")
//    private String plant;
//
//    @Column(name = "base")
//    private String base;
//
//    @Column(name = "shape")
//    private String shape;
//
//    @Column(name = "type")
//    private String type;
//
//    @Column(name = "type2")
//    private String type2;
//
//    @Column(name = "url")
//    private String url;
//
//    @Column(name = "shop")
//    private String shop;
//
//    @Column(name = "usd")
//    private String usd;
//
//    @Column(name = "pf")
//    private String pf;
//
//    @Column(name = "cri")
//    private String cri;
//
//    @Column(name = "r9")
//    private String r9;
//
//    @Column(name = "Rf")
//    private String Rf;
//
//    @Column(name = "Rg")
//    private String Rg;
//
//    @Column(name = "Duv")
//    private String Duv;
//
//    @Column(name = "flicker")
//    private String flicker;
//
//    @Column(name = "angle")
//    private String angle;
//
//    @Column(name = "switch")
//    private String switch;
//
//    @Column(name = "umin")
//    private String umin;
//
//    @Column(name = "drv")
//    private String drv;
//
//    @Column(name = "tmax")
//    private String tmax;
//
//    @Column(name = "date")
//    private String date;
//
//    @Column(name = "instruments")
//    private String instruments;
//
//    @Column(name = "add2")
//    private String add2;
//
//    @Column(name = "add3")
//    private String add3;
//
//    @Column(name = "add4")
//    private String add4;
//
//    @Column(name = "add5")
//    private String add5;
//
//    @Column(name = "cqs")
//    private String cqs;
//
//    @Column(name = "eq")
//    private String eq;
//
//    @Column(name = "act")
//    private String act;
//
//    @Column(name = "lamp_desc")
//    private String lamp_desc;

}
