package ru.surf.hackathon.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.surf.hackathon.backend.repository.entity.Led;

import java.util.Optional;

public interface LedRepository extends JpaRepository<Led, Integer> {

    Optional<Led> findByBarcode(Long barcode);
}
