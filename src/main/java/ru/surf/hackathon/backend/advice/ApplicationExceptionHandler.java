package ru.surf.hackathon.backend.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.surf.hackathon.backend.dto.ExceptionResponse;
import ru.surf.hackathon.backend.exception.BadBarcodeException;
import ru.surf.hackathon.backend.exception.NoSuchLedException;


@RestControllerAdvice
public class ApplicationExceptionHandler {

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(NoSuchLedException.class)
  public ExceptionResponse handleBusinessException(NoSuchLedException ex) {
    return new ExceptionResponse(ex);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(BadBarcodeException.class)
  public ExceptionResponse handleBusinessException(BadBarcodeException ex) {
    return new ExceptionResponse(ex);
  }
}
